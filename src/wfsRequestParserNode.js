"use strict";

module.exports = wfsRequestParserNode;

function wfsRequestParserNode(functionName, ...args) {
  require("jsdom").env("",function(err, windowMock) {
    if (err) {
      console.error(err);
      return;
    }

    var jQuery = require("jquery")(windowMock);
    var async = require('async');
    var _ = require('lodash');
    var navigator = {};
    navigator.userAgent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.98 Safari/537.36";
    var wfsRequestParser = require('../lib/node-script-1.1.10')(jQuery, _, windowMock, navigator);
    wfsRequestParser[functionName](...args);
  });
}
