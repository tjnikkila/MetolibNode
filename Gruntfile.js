module.exports = function(grunt) {
  // Project configuration.
  grunt.initConfig({
      pkg : grunt.file.readJSON('package.json'),
      concat : {
        libNodeJs : {
          files: {
            'lib/node-script-<%= pkg.version %>.js' : ['./lib/fmidev/metolib/src/intro.js', './src/wrapping/node-pre-parser.js', './lib/fmidev/metolib/src/utils.js', './lib/fmidev/metolib/src/wfsrequestparser.js', './src/wrapping/node-post-parser.js']
          }
        }
      },
      subgrunt: {
        target0: {
          projects: {
            './lib/fmidev/metolib': 'build'
          }
        }
      },
      clean : {
          files : ['lib/**/*','!lib/.gitignore']
      },
      gitclone: {
        clone: {
            options: {
                repository: 'https://github.com/fmidev/metolib.git',
                branch: 'master',
                directory: 'lib/fmidev/metolib'
            }
        }
      },
    }
  );

  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-subgrunt');
  grunt.loadNpmTasks('grunt-git');
  grunt.registerTask('default', ['clean:files', 'gitclone', 'subgrunt:target0', 'concat:libNodeJs']);
}
